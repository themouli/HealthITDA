package com.healthitda.android.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.healthitda.android.R;

/**
 * Created by mouli on 4/30/15.
 */
public class MCTSLinearLayout extends LinearLayout {
    private static final String TAG = "MCTSLinearLayout.class";
    private int childCount = -1;

    public MCTSLinearLayout(Context context) {
        super(context);
        init();
    }

    public MCTSLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MCTSLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        final View initialView = LayoutInflater.from(getContext()).inflate(R.layout.layout_mcts_item, this, false);
        initialView.findViewById(R.id.ivAdd).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addAnotherView();
            }
        });
        initialView.findViewById(R.id.ivRemove).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                removeThisView(initialView);
            }
        });
        this.addView(initialView);
    }

    private void addAnotherView() {
        View lastView = this.getChildAt(this.getChildCount() - 1);
        childCount = this.getChildCount();
        if(lastView.findViewById(R.id.ivAdd)!=null) {
            lastView.findViewById(R.id.ivAdd).setVisibility(View.GONE);
        }
        if(lastView.findViewById(R.id.ivRemove)!=null) {
            lastView.findViewById(R.id.ivRemove).setVisibility(View.VISIBLE);
        }
        final View newView = LayoutInflater.from(getContext()).inflate(R.layout.layout_mcts_item, this, false);
        if(newView.findViewById(R.id.ivAdd)!=null) {
            newView.findViewById(R.id.ivAdd).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    addAnotherView();
                }
            });
        }
        if(newView.findViewById(R.id.ivRemove)!=null) {
            newView.findViewById(R.id.ivRemove).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeThisView(newView);
                }
            });
        }
        this.addView(newView);
    }

    private void removeThisView(View v) {
        this.removeView(v);
    }

    public String getMCTSName() {
        StringBuilder builder = new StringBuilder();
        for(int i=0; i<this.getChildCount(); i++) {
            String mcts_id = ((EditText)this.getChildAt(i).findViewById(R.id.etMCTSName)).getText().toString();
            String anc_name = ((EditText)this.getChildAt(i).findViewById(R.id.etId)).getText().toString();
            if(!mcts_id.equals("") && !anc_name.equals("")) {
                builder.append("name:" + anc_name + " " + mcts_id + ", ");
            }
        }
        return builder.toString();
    }
}
