package com.healthitda.android.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;

/**
 * Created by mouli on 4/28/15.
 */
public class Mandal {
    private String name;
    private JSONArray phc;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JSONArray getPhc() {
        return phc;
    }

    public void setPhc(JSONArray phc) {
        this.phc = phc;
    }
}
